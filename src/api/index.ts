import { BASE_API_URL } from '../common/constants';
import { Comment, Post, User } from '../common/types';

export const getManyPosts = async (): Promise<Post[]> => {
  const res = await fetch(BASE_API_URL + 'posts', { method: 'GET' });
  const posts = await res.json();

  return posts;
};

export const getSinglePost = async (postId: number): Promise<Post> => {
  const res = await fetch(BASE_API_URL + `posts/${postId}`, {
    method: 'GET',
  });
  const post = await res.json();

  return post;
};

export const getPostComments = async (postId: number): Promise<Comment[]> => {
  const res = await fetch(BASE_API_URL + `posts/${postId}/comments`, {
    method: 'GET',
  });
  const comments = await res.json();

  return comments;
};

export const getSingleUser = async (userId: number): Promise<User> => {
  const res = await fetch(BASE_API_URL + `users/${userId}`, {
    method: 'GET',
  });
  const user = await res.json();

  return user;
};
